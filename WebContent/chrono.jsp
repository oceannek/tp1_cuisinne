<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<%@ include file="head.jsp" %>
    <h1 class="center">Minuteur</h1>
    <div class="center">
    
        <label >Heures : <br><input type="number" name="heure" id="heure" min="0" max="23" value="0"></label>
        <label >Minutes : <br><input type="number" name="minute" id="minute" min="0" max="59" value="0"></label>
        <label >Secondes : <br><input type="number" name="seconde" id="seconde" min="0" max="59" value="0"></label>
        <button onclick="start()">Demarrer</button>
        <button onclick="reset()">Arrêter</button>
        <br><br>

    
	    <div id="minuteur"></div>
	    
	    <br>	
    
    </div>
    
</body>
<%@ include file="footer.jsp" %>
</html>