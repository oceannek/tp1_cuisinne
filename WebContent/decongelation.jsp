<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<%@ include file="head.jsp"%>

<h1 class="center">Calculez vos temps de décongélations</h1>


<form method="post" class="center">
	<label>Masse à décongeler : (en kg) : <input type="text" name="masse" required></label>
	<button type="submit">Envoyer</button>
</form>
<br>

<% if (request.getParameter("masse") != null) {%>
	<table class="center">
	
		<thead>
			<tr>
				<th>Décongélation à l'air libre</th>
				<th>Décongélation au four</th>
			</tr>
		</thead>
	
		<tbody>
			<tr>
				<td><label> <input type="text" readonly value="${airLibre} h"></label></td>
				<td><label> <input type="text" value="${four} min" readonly></label></td>
			</tr>
		</tbody>
		
	</table>
	<br><br>
<%}%>

</body>
<%@ include file="footer.jsp"%>
</html>