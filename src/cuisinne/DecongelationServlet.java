package cuisinne;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DecongelationServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
     
    public DecongelationServlet() {
    
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    	//redirection vers decongelation.jsp
            req.getRequestDispatcher("decongelation.jsp").forward(req, resp);
    }
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        float masse =Float.parseFloat(req.getParameter("masse"));
        float airLibre = (masse*10); 
        float four =(masse*12);
        //rajoute l'attribue airLibre à sa value airLibre dans decongelation.jsp
        req.setAttribute("airLibre", airLibre);
        req.setAttribute("four", four);
        //pour ramener le formulaire sur la même page.
        doGet(req, resp);
    
}
    
}
